export const CREATE_NEW_QUIZ = 'newQuiz/SUBMIT';
export const CREATE_NEW_QUIZ_SUCCESS = 'newQuiz/SUBMIT_SUCCESS';
export const CREATE_NEW_QUIZ_ERROR = 'newQuiz/SUBMIT_ERROR';
