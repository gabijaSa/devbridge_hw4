import React from 'react';
import PropTypes from 'prop-types';
import Checkbox from '@material-ui/core/Checkbox/Checkbox';
import FormGroup from '@material-ui/core/FormGroup/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel/FormControlLabel';

class CheckAnswer extends React.Component {
    handleChange = (e, isChecked) => {
        const { value } = e.target;
        const values = this.props.value.slice(0);

        if (isChecked) {
            values.push(value);
        } else {
            const index = values.indexOf(i => i === value);
            values.splice(index);
        }

        this.props.onChange(values);
    };

    render() {
        return (
            <FormGroup>
                {this.props.answers.map(a => (
                    <FormControlLabel
                        key={a.id}
                        control={(
                            <Checkbox
                                onChange={this.handleChange}
                                value={a.text}
                            />
                        )}
                        label={a.text}
                    />
                ))}
            </FormGroup>
        );
    }
}

CheckAnswer.propTypes = {
    answers: PropTypes.arrayOf(PropTypes.shape({
        id: PropTypes.number,
        text: PropTypes.string,
    })).isRequired,
    onChange: PropTypes.func.isRequired,
    value: PropTypes.arrayOf(PropTypes.string).isRequired,
};

export default CheckAnswer;
