const imitateApiDelay = contentToServe => new Promise((resolve) => {
    setTimeout(() => resolve(contentToServe), 1500);
});

function ajax(url, options) {
    return new Promise((resolve, reject) => {
        fetch(url, options)
            .then(response => response.json()
                .then(json => ({
                    status: response.status,
                    json,
                })))
            .then((response) => {
                if (response.status >= 400) {
                    reject(response.json);
                } else {
                    resolve(response.json);
                }
            }, error => reject(error));
    });
}

export default class Api {
    static get(url) {
        const options = {
            method: 'GET',
        };

        // return ajax(url, options);
        return imitateApiDelay(ajax(url, options));
    }

    static post = (url, data) => {
        const options = {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        // return ajax(url, options);
        return imitateApiDelay(ajax(url, options));
    };
}
