import {
    all,
    put,
    takeLatest,
} from 'redux-saga/effects';

import { CREATE_NEW_QUIZ } from 'state-management/constants/newQuiz';
import {
    createNewQuizSuccess,
    createNewQuizError,
} from 'state-management/actions/newQuiz';
import Storage from 'utils/storage';

const QUIZ_STORAGE = 'quizzes';

function* createNewQuiz({ quiz }) {
    try {
        const oldQuizzes = yield Storage.load(QUIZ_STORAGE, []);
        const newQuizzes = [
            ...oldQuizzes,
            quiz,
        ];
        yield Storage.save(QUIZ_STORAGE, newQuizzes);
        yield put(createNewQuizSuccess(quiz));
    } catch (e) {
        const error = (e && e.error) ? e.error : 'System error';
        yield put(createNewQuizError(error));
    }
}


function* newQuizSaga() {
    yield all([
        takeLatest(CREATE_NEW_QUIZ, createNewQuiz),
    ]);
}

export default newQuizSaga;
