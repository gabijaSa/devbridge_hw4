import { all, fork } from 'redux-saga/effects';

import feedbackSaga from 'state-management/sagas/feedback';
import quizSaga from 'state-management/sagas/quiz';
import newQuizSaga from 'state-management/sagas/newQuiz';
import resultsSaga from 'state-management/sagas/results';

export default function* root() {
    yield all([
        fork(feedbackSaga),
        fork(quizSaga),
        fork(resultsSaga),
        fork(newQuizSaga),
    ]);
}
