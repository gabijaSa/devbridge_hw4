import { all, put, takeLatest } from 'redux-saga/effects';
import { push } from 'react-router-redux';

import { GET_QUIZ, QUIZ_VIEW_URL } from 'state-management/constants/quiz';
import { getQuizSuccess, getQuizError } from 'state-management/actions/quiz';
import Api from 'utils/api';
import Storage from 'utils/storage';

function* getQuiz({ code }) {
    try {
        try {
            const quiz = yield Api.get(QUIZ_VIEW_URL(code));
            yield put(getQuizSuccess(quiz));
            yield put(push('/quiz'));
        } catch (e) {
            const quizzes = yield Storage.load('quizzes', []);
            let quiz = null;
            quizzes.forEach((q) => {
                if (q.code === code) {
                    quiz = q;
                }
            });
            yield put(getQuizSuccess(quiz));
            yield put(push('/quiz'));
        }
    } catch (e) {
        const error = (e && e.error) ? e.error : 'System error';
        yield put(getQuizError(error));
    }
}

function* quizSaga() {
    yield all([
        takeLatest(GET_QUIZ, getQuiz),
    ]);
}

export default quizSaga;
