export const SUBMIT_RESULTS = 'results/SUBMIT';
export const SUBMIT_RESULTS_SUCCESS = 'results/SUBMIT_SUCCESS';
export const SUBMIT_RESULTS_ERROR = 'results/SUBMIT_ERROR';
export const CLEAR_RESULTS = 'results/CLEAR';
export const UPDATE_VIEWS = 'results/UPDATE_VIEWS';
export const GET_RESULTS = 'results/GET';
export const GET_RESULTS_SUCCESS = 'results/GET_SUCCESS';
export const GET_RESULTS_ERROR = 'results/GET_ERROR';
