import React from 'react';
import PropTypes from 'prop-types';
import { Header, QuizCreateForm, TextDialog } from 'components';
import connect from 'react-redux/es/connect/connect';

import './quizGenerator.scss';

import {
    createNewQuiz as createNewQuizAction,
} from 'state-management/actions/newQuiz';


class QuizGenerator extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            open: false,
            quiz: {
                id: 'initial',
                questions: [],
            },
        };
    }

    addQuestion = (type) => {
        const { quiz } = this.state;

        const id = quiz.questions.length === 0
            ? 0 : quiz.questions[quiz.questions.length - 1].id + 1;

        quiz.questions.push({
            id,
            type,
            text: '',
            answers: [],
        });

        this.setState({
            quiz,
        });
    };

    changeField = (e) => {
        const { quiz } = this.state;
        const { name, value } = e.target;

        quiz[name] = value;

        this.setState({
            quiz,
        });
    };

    changeQuestion = (question) => {
        const { quiz } = this.state;
        const index = quiz.questions.findIndex(q => q.id === question.id);

        quiz.questions.splice(index, 1, question);

        this.setState({
            quiz,
        });
    };

    handleClose = () => {
        this.setState({ open: false });
    };

    handleSubmit = () => {
        this.props.createNewQuiz({
            code: this.state.quiz.code,
            title: this.state.quiz.title,
            description: this.state.quiz.description,
            questions: this.state.quiz.questions,
        });
    };

    removeQuestion = (id) => {
        const { quiz } = this.state;
        const index = quiz.questions.findIndex(q => q.id === id);

        quiz.questions.splice(index, 1);

        this.setState({
            quiz,
        });
    };

    render() {
        const { questions } = this.state.quiz;

        return (
            <React.Fragment>
                <Header
                    addQuestion={this.addQuestion}
                    title="Quiz Generator"
                />
                <QuizCreateForm
                    questions={questions}
                    changeField={this.changeField}
                    changeQuestion={this.changeQuestion}
                    handleSubmit={this.handleSubmit}
                    removeQuestion={this.removeQuestion}
                />
                <TextDialog
                    handleClose={this.handleClose}
                    open={this.state.open}
                    text={JSON.stringify(this.state.quiz, null, 2)}
                />
            </React.Fragment>
        );
    }
}

const mapDispatchToProps = dispatch => ({
    createNewQuiz: result => dispatch(createNewQuizAction(result)),
});

QuizGenerator.propTypes = {
    createNewQuiz: PropTypes.func.isRequired,
};

export default connect(null, mapDispatchToProps)(QuizGenerator);
