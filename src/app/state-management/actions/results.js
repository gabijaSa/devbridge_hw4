import {
    SUBMIT_RESULTS,
    SUBMIT_RESULTS_SUCCESS,
    SUBMIT_RESULTS_ERROR,
    CLEAR_RESULTS,
    UPDATE_VIEWS,
    GET_RESULTS,
    GET_RESULTS_SUCCESS,
    GET_RESULTS_ERROR,
} from 'state-management/constants/results';

export const submitResults = results => ({
    type: SUBMIT_RESULTS,
    results,
});

export const submitResultsSuccess = results => ({
    type: SUBMIT_RESULTS_SUCCESS,
    results,
});

export const submitResultsError = error => ({
    type: SUBMIT_RESULTS_ERROR,
    error,
});

export const clearResults = () => ({
    type: CLEAR_RESULTS,
});

export const updateViews = () => ({
    type: UPDATE_VIEWS,
});

export const getResults = () => ({
    type: GET_RESULTS,
});

export const getResultsSuccess = results => ({
    type: GET_RESULTS_SUCCESS,
    results,
});

export const getResultsError = error => ({
    type: GET_RESULTS_ERROR,
    error,
});
