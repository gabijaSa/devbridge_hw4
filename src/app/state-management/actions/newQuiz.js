import {
    CREATE_NEW_QUIZ,
    CREATE_NEW_QUIZ_SUCCESS,
    CREATE_NEW_QUIZ_ERROR,
} from 'state-management/constants/newQuiz';

export const createNewQuiz = quiz => ({
    type: CREATE_NEW_QUIZ,
    quiz,
});

export const createNewQuizSuccess = quiz => ({
    type: CREATE_NEW_QUIZ_SUCCESS,
    quiz,
});

export const createNewQuizError = error => ({
    type: CREATE_NEW_QUIZ_ERROR,
    error,
});
