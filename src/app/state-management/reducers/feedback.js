import {
    POST_FEEDBACK,
    POST_FEEDBACK_SUCCESS,
    POST_FEEDBACK_ERROR,
} from 'state-management/constants/feedback';

const initialState = {
    isLoading: false,
    error: '',
    feedback: null,
};

export default function feedbackReducer(state = initialState, action = {}) {
    switch (action.type) {
    case POST_FEEDBACK: return {
        ...state,
        isLoading: true,
        error: '',
        feedback: action.feedback,
    };
    case POST_FEEDBACK_SUCCESS: return {
        ...state,
        isLoading: false,
        error: '',
        feedback: null,
    };
    case POST_FEEDBACK_ERROR: return {
        ...state,
        isLoading: false,
        error: action.error,
    };
    default:
        return state;
    }
}
