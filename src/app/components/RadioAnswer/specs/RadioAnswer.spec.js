import React from 'react';
import { shallow } from 'enzyme';
import quiz from 'data/quiz';
import RadioAnswer from '../RadioAnswer';

describe('RadioAnswer', () => {
    it('should send new value via callback', () => {
        const functionCalled = jest.fn();
        const radioAnswer = shallow(
            <RadioAnswer
                answers={quiz.questions.find(q => q.id === 1).answers}
                onChange={functionCalled}
            />,
        );

        const expectedCalls = [
            ['1'],
            ['2'],
        ];

        const radioGroup = radioAnswer.find('.radio-group');
        radioGroup.simulate('change', { target: { value: '1' } });
        radioGroup.simulate('change', { target: { value: '2' } });

        expect(functionCalled.mock.calls).toEqual(expectedCalls);
    });
});
