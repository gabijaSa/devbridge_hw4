import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';

import feedback from 'state-management/reducers/feedback';
import quiz from 'state-management/reducers/quiz';
import newQuiz from 'state-management/reducers/newQuiz';
import answers from 'state-management/reducers/answers';
import results from 'state-management/reducers/results';

export default combineReducers({
    router: routerReducer,
    feedback,
    quiz,
    newQuiz,
    answers,
    results,
});
