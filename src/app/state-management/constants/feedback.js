export const POST_FEEDBACK_URL = '/api/feedback';

export const POST_FEEDBACK = 'feedback/POST_FEEDBACK';
export const POST_FEEDBACK_SUCCESS = 'feedback/POST_FEEDBACK_SUCCESS';
export const POST_FEEDBACK_ERROR = 'feedback/POST_FEEDBACK_ERROR';
