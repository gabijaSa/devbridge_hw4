const storeItem = (itemType, item) => {
    localStorage.setItem(itemType, JSON.stringify(item));
};

const getItem = itemType => JSON.parse(localStorage.getItem(itemType));

const imitateApiDelay = contentToServe => new Promise((resolve) => {
    setTimeout(() => resolve(contentToServe), 1500);
});

export default class Storage {
    static save(itemType, item) {
        storeItem(itemType, item);
        return imitateApiDelay(item);
    }

    static load(itemType, defaultItem = {}) {
        const item = getItem(itemType) || defaultItem;
        return imitateApiDelay(item);
    }
}
