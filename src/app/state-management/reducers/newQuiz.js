import {
    CREATE_NEW_QUIZ,
    CREATE_NEW_QUIZ_SUCCESS,
    CREATE_NEW_QUIZ_ERROR,
} from 'state-management/constants/newQuiz';

const initialState = {
    quizzes: [],
    error: null,
    isLoading: false,
};

export default function newQuizReducer(state = initialState, action = {}) {
    switch (action.type) {
    case CREATE_NEW_QUIZ: return {
        ...state,
        isLoading: true,
    };
    case CREATE_NEW_QUIZ_SUCCESS: return {
        ...state,
        quizzes: [
            ...state.quizzes,
            action.quiz,
        ],
        error: null,
        isLoading: false,
    };
    case CREATE_NEW_QUIZ_ERROR: return {
        ...state,
        error: action.error,
        isLoading: false,
    };
    default:
        return state;
    }
}
