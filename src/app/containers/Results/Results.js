import React, { Component } from 'react';
import PropTypes from 'prop-types';
import connect from 'react-redux/es/connect/connect';
import { Link } from 'react-router-dom';
import Button from '@material-ui/core/Button/Button';
import {
    clearResults as clearResultsAction,
    updateViews as updateViewsAction,
    getResults as getResultsAction,
} from 'state-management/actions/results';
import { Header, ResultsList } from 'components';

class Results extends Component {
    componentDidMount() {
        const { getResults, updateViews, isLoadedOnce } = this.props;
        if (!isLoadedOnce) {
            getResults();
        }
        updateViews();
    }

    render() {
        const {
            isLoading,
            resultsList,
            views,
            clearResults,
        } = this.props;
        return (
            <React.Fragment>
                <Header title="Results" />
                {isLoading ? (
                    'Loading...'
                ) : (
                    <ResultsList resultsList={resultsList} views={views} />
                )}
                <Button
                    variant="contained"
                    color="primary"
                    onClick={clearResults}
                >
                    Clear list
                </Button>
                <Link to="/quiz">
                    <Button variant="contained" type="button">
                        Back to quiz
                    </Button>
                </Link>
                <Link to="/">
                    <Button variant="contained" type="button">
                        Go home
                    </Button>
                </Link>
            </React.Fragment>
        );
    }
}

Results.propTypes = {
    resultsList: PropTypes.arrayOf(PropTypes.shape({
        quizId: PropTypes.string.isRequired,
        quizCode: PropTypes.string.isRequired,
        correctAnswers: PropTypes.number.isRequired,
        totalAnswers: PropTypes.number.isRequired,
        submitTime: PropTypes.string.isRequired,
    })).isRequired,
    views: PropTypes.number.isRequired,
    clearResults: PropTypes.func.isRequired,
    updateViews: PropTypes.func.isRequired,
    getResults: PropTypes.func.isRequired,
    isLoading: PropTypes.bool.isRequired,
    isLoadedOnce: PropTypes.bool.isRequired,
};

const mapStateToProps = state => ({
    resultsList: state.results.list,
    views: state.results.views,
    isLoading: state.results.isLoading,
    isLoadedOnce: state.results.isLoadedOnce,
});

const mapDispatchToProps = dispatch => ({
    clearResults: () => dispatch(clearResultsAction()),
    updateViews: () => dispatch(updateViewsAction()),
    getResults: () => dispatch(getResultsAction()),
});

export default connect(mapStateToProps, mapDispatchToProps)(Results);
